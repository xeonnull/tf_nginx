FROM debian:9 as builder

RUN apt-get update &&\
    apt-get install -y make gcc wget libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev libgetopt-complete-perl

RUN wget https://openresty.org/download/openresty-1.19.3.1.tar.gz &&\
    tar xfz openresty-1.19.3.1.tar.gz

RUN cd /openresty-1.19.3.1 &&\
    ./configure &&\
    make &&\
    make install

FROM debian:9

WORKDIR /usr/local/openresty

COPY --from=builder /usr/local/openresty .

COPY --from=builder /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /usr/lib/x86_64-linux-gnu/libssl.so.1.1
COPY --from=builder /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1

ENTRYPOINT ["/usr/local/openresty/nginx/sbin/nginx", "-g", "daemon off;"]